#!/bin/bash
set -ex

# The command is the first argument or an empty
# string if the first argument is unbound
COMMAND=${1:-}

validate() {
  su steam -c "/home/steam/steamcmd/steamcmd.sh \
    +login anonymous \
    +force_install_dir $STEAMAPPDIR \
    +app_update 740 validate \
    +quit"
  }

start_server() {
  su steam -c "$STEAMAPPDIR/srcds_run \
    -game csgo \
    -console \
    -autoupdate \
    -steam_dir $STEAMAPPDIR \
    -steamcmd_script /home/steam/csgo_update.txt \
    -usercon \
    +fps_max $FPSMAX \
    -tickrate $TICKRATE \
    -port $PORT \
    -tv_port $TV_PORT \
    -maxplayers_override $MAXPLAYERS \
    +game_type $GAMETYPE \
    +game_mode $GAMEMODE \
    +mapgroup $MAPGROUP \
    +map $STARTMAP \
    +sv_setsteamaccount $TOKEN \
    +rcon_password $RCONPW \
    +sv_password $PW \
    +sv_region $REGION \
    $@"
  }

# TODO test whether csgo is installed or not
if [[ "$COMMAND" == "install-mods" ]]; then
  apt-get update
  apt-get install -y --no-install-recommends --no-install-suggests \
    libxml2-utils \
    wget

  [ -z "$METAMOD_URL" ] && METAMOD_URL=`wget -qO- https://www.metamodsource.net/downloads.php\?branch\=$METAMOD_BRANCH | xmllint --html --xpath 'string(//a/*[text()="Linux"]/../@href)' - 2>/dev/null`
  echo "Downloading newest metamod ($METAMOD_BRANCH) from url: $METAMOD_URL"
  wget -qO- $METAMOD_URL | tar xvzC $STEAMAPPDIR/csgo

  [ -z "$SOURCEMOD_URL" ] && SOURCEMOD_URL=`wget -qO- https://sm.alliedmods.net/downloads.php\?branch\=$SOURCEMOD_BRANCH | xmllint --html --xpath 'string(//a/*[text()="Linux"]/../@href)' - 2>/dev/null`
  echo "Downloading newest sourcemod ($SOURCEMOD_BRANCH) from url: $SOURCEMOD_URL"
  wget -qO- $SOURCEMOD_URL | tar xvzC $STEAMAPPDIR/csgo

  apt-get remove --purge -y \
    libxml2-utils \
    wget

  apt-get clean autoclean
  apt-get autoremove -y
  rm -rf /var/lib/apt/lists/*
elif [[ "$COMMAND" == "install-zip-url" ]]; then
  echo "Installing a mod zip file from $2"
  apt-get update
  apt-get install -y --no-install-recommends --no-install-suggests \
    unzip \
    wget

  TMPFILE=`mktemp`
  wget "$2" -O $TMPFILE
  unzip -d $STEAMAPPDIR/csgo $TMPFILE
  rm $TMPFILE

  apt-get remove --purge -y \
    unzip \
    wget

  apt-get clean autoclean
  apt-get autoremove -y
  rm -rf /var/lib/apt/lists/*
elif [[ "$COMMAND" == "install-targz-url" ]]; then
  echo "Installing a mod tar.gz file from $2"
  apt-get update
  apt-get install -y --no-install-recommends --no-install-suggests \
    wget

  wget -qO- $2 | tar xvzC $STEAMAPPDIR/csgo

  apt-get remove --purge -y \
    wget

  apt-get clean autoclean
  apt-get autoremove -y
  rm -rf /var/lib/apt/lists/*
elif [[ "$COMMAND" == "edit-sm-admins" ]]; then
  apt-get update
  apt-get install -y --no-install-recommends --no-install-suggests \
    vim

  su steam -c "vim /home/steam/csgo-dedicated/csgo/addons/sourcemod/configs/admins_simple.ini"

  apt-get remove --purge -y \
    vim

  apt-get clean autoclean
  apt-get autoremove -y
  rm -rf /var/lib/apt/lists/*
elif [[ "$COMMAND" == "install" ]]; then
  validate
elif [[ "$COMMAND" == "novalidate" ]]; then
  shift
  start_server $@
else
  validate
  start_server $@
fi
