# SteamUtils

_A collection of dockerfiles for SteamCMD and CS:GO dedicated server_

## Usage

### SteamCMD

Start the `steamcmd` docker image

    docker run -it niklashh/steamcmd:latest

### CS:GO

#### Installing

Create a named volume for the game files

    docker volume create csgo-dedicated

Install the dedicated server in the volume

    docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest install

#### Running

Update, validate and start the dedicated server (slower, but recommended)

    docker run --rm -e TOKEN=<your game server token> --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest

> _Note: the game server token is required only when actually starting the server_

Just start the dedicated server (faster)

    ... niklashh/csgo:latest novalidate

> If you see a message like
> `Your server is out of date and will be shutdown during hibernation or changelevel, whichever comes first.`
> then you need to validate first (which is the default option without parameters)

> Arguments to the command will be parsed
> by the server `srcds_run` -srcipt
>
> See the default arguments in [`docker-entrypoint.sh`](/csgo/docker-entrypoint.sh)

Editing the server files

    docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated --entrypoint bash -it niklashh/csgo:latest
    cd csgo/addons/sourcemod/plugins
    cd csgo/cfg/sourcemod

> You can have the server running and edit the server files simultaneously

Writing quit in the terminal only restarts the server, you must press ctrl-c

#### Modding

Install metamod and sourcemod

    docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest install-mods

> Change env vars METAMOD_BRANCH and SOURCEMOD_BRANCH to master
> if you want the development versions installed
> `docker run -e METAMOD_BRANCH=master ...`

At the time of writing this the sourcemod download website is broken and the
linux button does nothing. To mitigate the issue, you can pass in the URL to the
sourcemod download file manually

> You can find the download files [here](https://sm.alliedmods.net/smdrop/)

    docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -e SOURCEMOD_URL="https://sm.alliedmods.net/smdrop/1.10/sourcemod-1.10.0-git6492-linux.tar.gz" -it niklashh/csgo install-mods

Install custom mods (zip files)

    docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest install-zip-url "https://mod-url"

> Example for installing [MaraBot](https://forums.alliedmods.net/showthread.php?t=293737)
> from the attached file in the thread:
>
> ```shell
> docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest install-zip-url "https://forums.alliedmods.net/attachment.php?attachmentid=160855&d=1487107916"
> ```

> You can have the server running and install mods at the same time

Or tar.gz

    docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest install-targz-url "https://mod-url"

> The archives will be extracted under /home/steam/csgo-dedicated/csgo

##### Editing sourcemod admins

Edit `csgo/addons/sourcemod/configs/admins_simple.ini` inside the volume (and in
the container) with `vim` and add your admins according to the
[guide](<https://wiki.alliedmods.net/Adding_Admins_(SourceMod)>)

    docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest edit-sm-admins

### Bhop setup

- https://timer.shav.it/
  - https://users.alliedmods.net/~drifter/builds/dhooks/
- https://github.com/Franc1sco/FixHintColorMessages

## TODO

- Ensure game files are installed before allowing user to
  install mods
- Add help to entrypoint
- Install mods as user (ID 1000)
